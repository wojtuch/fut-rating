import math, sys

def calc(ratings):
    sum_ratings = 1.0 * sum(ratings)
    # print(sum_ratings)
    avg = sum_ratings / 11
    # print(avg)
    total_excess = sum([r - avg for r in ratings if r-avg > 0.0])
    # print(total_excess)
    new_total = sum_ratings + total_excess
    # print(new_total)
    new_total = round(new_total)
    # print(new_total)
    new_avg = math.floor(new_total / 11)
    print(new_avg)

if __name__ == '__main__':
    ratings_str = sys.argv[1]
    ratings = [int(r) for r in ratings_str.split(",")]
    calc(ratings)
